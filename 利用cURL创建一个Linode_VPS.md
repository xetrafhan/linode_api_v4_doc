# 利用cURL创建一个Linode_VPS
> by William Smith; last updated May 22nd, 2017

创建 Linode 需要您进行身份验证。在继续之前, 请确保您已经通过[利用cURL测试API]()进行了测试, 因为您将需要一个个人授权令牌(PTO)才能继续。

在您可以创建 Linode 之前, 您需要选择一个区域、一个类型和一个系统。

# 选择区域

区域是 Linodes VPS 的物理位置。若要检索可用区域的列表, 可以使用`/regions` API。`cURL`要对此`API`进行调用, 请运行以下命令:

```
curl https://api.linode.com/v4/regions
```

请注意, 由于区域列表是公共信息, 因此您不需要经过身份验证。上面的命令将返回一个 JSON 对象, 如下所示:

```
{
  "data": [
      {
          "id": "us-east-1a",
          "label": "Newark, NJ",
          "country": "US"
      }
      /* and so on */
  ],
  "page": 1,
  "pages": 1,
  "results": 7
}
```

区域列表不言自明:
区域的地理位置在 `label` 字段中提供。`id` 字段是唯一的 id, 您将使用它来引用要选择的区域。对于这个例子, 我们将使用 "纽约, 新泽西州"，地区 ID 为 "us-east-1a"。

# 选择类型
一旦您有了一个区域, 下一步就是选择一个 Linode 类型。类型将决定VPS的性能 (如内存、存储空间和网络传输)。运行以下`cURL`命令可检索所有可用的 Linode VPS规格:

```
curl https://api.linode.com/v4/regions
```

请注意, 由于区域列表是公共信息, 因此您不需要经过身份验证。上面的命令将返回一个 JSON 对象, 如下所示:

```
{
  "data": [
      {
          "id": "g5-standard-1",
          "label": "Linode 2048",
          "vcpus": 1,
          "network_out": 125,
          "disk": 24576,
          "price_hourly": 1,
          "class": "standard",
          "memory": 2048,
          "price_monthly": 1000,
          "backups_option": {
            "price_hourly": 0.004,
            "price_monthly": 2.5
          },
          "transfer": 2000
      }
      /* and so on */
  ],
  "page": 1,
  "pages": 1,
  "results": 1
}
```

列表中的每个条目都表示可用的 Linode 类型。您可以查看每个计划的详细信息, 如: cpu的数量、内存、磁盘空间、月价格等。

有关每个字段的解释，[请参考这里](https://developers.linode.com/v4/reference/endpoints/linode/types)。 选择要启动的类型后，请记下其`ID`并继续下一步。


# 选择系统

现在, 您需要选择一个 Linux 发行版来部署到您的新 Linode。就像选择一个类型和一个区域一样, 向 API 发出一个调用, 这次是一个可用发行版的列表:

```
curl https://api.linode.com/v4/linode/distributions
```

这将为您提供类似以下发行版的列表:

```
{
  "data": [
      {
          "id": "linode/debian8",
          "label": "Debian 8",
          "vendor": "Debian",
          "architecture": "x86_64",
          "recommended": true,
          "disk_minimum": 900,
          "updated": "2015-04-27T16:26:41"
      }
      /* and so on */
  ],
  "page": 1,
  "pages": 1,
  "results": 1
}
```

有关每个字段的详细信息, [请参阅这里](https://developers.linode.com/v4/reference/endpoints/linode/distributions)。对于本例, 我们将使用 Debian 8。


# 创建新的VPS

现在, 您已经选择了一个区域、类型和系统, 您已经准备好启动一个新的VPS! 上面的 API 调用未经身份验证, 因为它们只返回公开可见的信息。但是, 启动VPS与就意味着我们要在您的账户上氪金了, 所以此调用必须先进行身份验证。

在运行它之前, 您需要在`cURL`的参数中加入您的授权令牌 (您可以把授权令牌设置为环境变量`$TOKEN`，然后重启终端)。如果您还没有授权令牌, 请先阅读[利用cURL测试API]()

同时还要为新的VPS(这里我们使用我们提前设置好的环境变量`$root_pass`) 设置`root`密码

正如您所看到的, 区域、类型和系统 都是在 `HTTP POST BODY` 的JSON中指定的, 可以根据需要进行更改, 以便将 Linodes 不同型号的部署到不同的地区。自定义以下`cURL`命令并在准备部署时运行:

```
curl -X POST https://api.linode.com/v4/linode/instances -d '{"type": "g5-standard-1", "region": "us-east-1a", "distribution": "linode/debian8", "root_pass": "$root_pass", "label": "prod-1"}' \
  -H "Authorization: Bearer $TOKEN" -H "Content-type: application/json"
```

如果全部成功, 您应该得到一个响应对象, 其中详细介绍了新创建的 Linode, 如下所示:

```
{
  "id": 123456,
  "transfer_total": 2000,
  "label": "prod-1",
  "status": "provisioning",
  "group": "",
  "updated": "2016-11-10T19:01:12",
  "created": "2016-11-10T19:01:12",
  "hypervisor": "kvm",
  "ipv4": "97.107.143.56",
  "ipv6": "2600:3c03::f03c:91ff:fe0a:18ab/64",
  "region": "us-east-1a",
  "type":  "g5-standard-1",
  "distribution": "linode/debian8",
  "alerts": {
    "cpu":  90,
    "transfer_out": 10,
    "io": 10000,
    "transfer_quota": 80,
    "transfer_in": 10
  },
  "backups": {
    "enabled": false,
    "schedule": {
       "day": null,
       "window": null
    }
  }
}
```

上述响应包含有关新创建的 VPS 的大量详细信息, 包括 IP 地址、警报、区域信息和meta数据如创建日期和时间。有关所有返回字段的信息, 请参见更多文档。存下`id`字段, 因为您将需要它来进行下一步。


# 启动您的VPS
在您可以使用新的 Linode 之前, 您需要将其启动。采用前一个 API 调用返回的 `id` , 并将其替换为以下`cURL`命令中的 `$linode_id`。还要记住用您的授权令牌替换`$TOKEN`, 如前面的 API 调用一样。

```
curl -X POST https://api.linode.com/v4/linode/instances/$linode_id/boot \
  -H "Authorization: Bearer $TOKEN"
```

您将收到一个空的响应 ({}), 这是预期的行为。如果状态代码为 `200`, 则它有效。现在, 您可以监视 Linode 的`state`字段从 `booting` 更改为 `running`:

```
curl https://api.linode.com/v4/linode/instances/$linode_id \
  -H "Authorization: Bearer $TOKEN"
```
```
{
  "linode": {
      "id": 123456,
      "label": "prod-1",
      "status": "running",
      /* and so on */
  }
}
```

# 使用SSH登陆您的VPS
```
ssh root@$public_ip
```

