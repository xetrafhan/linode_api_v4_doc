# OAUTH登录

* 调用任何接口之前，都需要进行身份验证。我们使用基本的OAuth工作流程，您可以通过向我们注册您的应用程序来创建与Linode集成的应用程序。然后，您使用OAuth代表用户进行身份验证，以请求访问其帐户中的资源。

* `The Access Code`
    
    按照OAUTH的规矩，在开始使用API之前，有两个用户身份验证的步骤，您首先要请求到一个`Access Code`，然后就可以用其得到`Access Token`。（这里原文说给用户提供两种方法，但是他只写了一种）

# 方法1：通过打开浏览器页面

* 这个方法适合基于Web的应用程序。你可以引导你的用户去访问一个类似于这样的URL：
```
    https://login.linode.com/oauth/authorize?client_id=client_id
```

用户完成登录后, 授权会在Session销毁前一直保持有效。一旦用户接受了您的访问请求，我们颁发给您`Access Code`。然后您就可以用其拿到`Access Token`。

上面的`GET`请求支持更多的参数。比如：如果您在请求中提供了`redirect_uri`，授权后浏览器将重定向到该URI。如果没有这个参数，页面将跳转与您的OAuth客户端应用程序关联的`默认重定向URI`。

授权页面跳转后，会返回两个值：`code`和`state`。`state`参数将会原封不动地返回给您。这可以确保是您的应用程序启动的OAuth流程，而不是其他软件通过您的`client_id`手动导航到的[https://login.linode.com]()。`code`是最重要的参数，它的值是您所要的`Access Code`。

这个请求可以包含下列`GET`参数：
参数说明

| 参数 | 说明 |
|----------|--------------------|
| client_id | 必要参数，您的应用程序的(`client_id`)，如果您没有，请到Linode注册您的应用程序 |
| scope | 您需要使用逗号分隔的OAuth范围列表。 A comma-delimited list of OAuth scopes you need. |
| redirect_uri | 用户登录并授权后的跳转URI，这个URI应该与您在注册您的应用程序时提供的`重定向URI`相匹配。如果您将`http://example.org`注册为`重定向URI`，则此参数必须以`http://example.org`开头。|
| response_type | 必要参数. 参数可以是 `code` [for an OAUTH Authorization Code Grant](https://tools.ietf.org/html/rfc6749#section-4.1) 或者是 `token` [for an OAuth Implcit Grant token](https://tools.ietf.org/html/rfc6749#section-4.2). |
| state | 这个参数可以是任何值，我们会在授权跳转之后返回给您 |


* `The Access Token`

一旦用户登录到Linode并且已经收到访问代码，您将需要交换访问代码以获得授权令牌。您可以通过发出以下`HTTP``POST`请求来完成此操作：
```
https://login.linode.com/oauth/token
```

设置`POST` `Headers`的`Content-Type`为`application/x-www-form-urlencoded`或`multipart/form-data`的形式发出此请求，并在`POST Body`中包含以下参数：

参数说明：

| 参数名 | 描述 |
|--------|-----|
| client_id| 应用程序的`client ID` |
| client_secret | 应用程序的`client secret` |
| code | `The Access Code` |

响应示例：
```
{
  "scope": "linodes:create",
  "access_token": "03d084436a6c91fbafd5c4b20c82e5056a2e9ce1635920c30dc8d81dc7a6665c"
  "token_type": "bearer",
  "expires_in": 7200,
}
```

请注意，我们在这里包含`scope`。将来，我们可能会改变登录流程以允许您用户拒绝您访问特定的`scope`。您应该将`scope`的范围视为用户给您的所有权限，即使您所请求的权限与其不同。这个请求返回给您了`access_token`。在未来调用Linode API时，请将该`Access Token`放在`HTTP``Header`里面。
```
Authorization: Bearer 03d084436a6c91fbafd5c4b20c82e5056a2e9ce1635920c30dc8d81dc7a6665c
```

* `scope`参数 (`OAuth Scopes`)

上面的登录流程只给了您最低级别的权限，也就是说，您只能看到他们的用户名和电子邮件地址。如果您需要更多访问权限，则需要将`OAuth范围`添加到查询字符串中。 OAuth范围定义您的`Access Token`的访问权限级别。您可以通过将由逗号分隔的范围列表`scope = a，b，c`将范围添加到`GET`参数中

`scope`遵循`resource:access`的形式，其中对特定资源的访问受限于用户授予应用程序的级别。

* 您可以请求任何给定资源的以下访问级别：
    * view - 最低级别
    * modify
    * create
    * delete - 最高级别


除了访问您请求的水平，您将获得它下面的每个访问级别。例如，请求`modify`的权限也将给予您`view`权限，请求`delete`访问将授予最高权限。您也可以使用`resource:*`来请求最高权限。

* 文档中的所有API都会告诉您其所需要的权限级别，比如(`OAuth scopes: linodes:view`), 您可以使用`*`来请求用户的所有权限，不过这可不是个好主意，请您按用户需要请求适当等级的权限。

# 下一页：[pagination: 导航]()