# 利用cURL测试API
> 由威廉史密斯; 最后更新2017年5月22日

`cURL`是一种简单且受欢迎的命令行工具, 它允许您执行各种类型的 `HTTP`请求。它可能已经安装在您的系统, 您可以使用 `curl --version`在您的shell中检查。一旦你确认你已经安装`cURL`, 我们可以在shell中用它来测试舒适的 Linode API。

# 未经验证的请求
您可以对 `API` 上的各种资源执行匿名 `HTTP` 请求。Linode API文档中会告诉您哪些API可以匿名调用。对于缺少此指示器的 `API`, 您可以使用`cURL`来测试它们, 而无需执行任何其他步骤。例如, 我们可以列出支持的系统:

```
    curl https://api.linode.com/v4/linode/distributions
```

将给您这样的响应:

```
{
  "distributions": [
      {
          "updated": "2014-10-24T15:48:04",
          "id": "linode/ubuntu14.10",
          "label": "Ubuntu 14.10",
          "disk_minimum": 650,
          "recommended": true,
          "vendor": "Ubuntu",
          "architecture": "x86_64"
      }
      /* and so on */
  ],
  "page": 1,
  "pages": 2,
  "results": 34
}
```

# 经过身份验证的请求

对于大多数请求, 您将必须通过身份验证。现在, 我们将使用个人访问令牌(PTO)来演示。您同时可以参考文档：[授权.md]()。

要生成个人访问令牌(PTO), [请访问我们的新控制台](https://cloud.linode.com/profile/tokens)。这些令牌可用于使用您的Linode帐户进行已认证的API请求，并且可以完全访问所有`OAuth Scopes`。你只会看到完整的 OAuth 令牌一次, 所以一定要把记下来。如果你是在`shell`中, 你可以这样做:

```
token="that token"
```

# 身份验证的 HTTP Header

现在, 您可以通过添加`-H "Authorization: Bearer $token"`, 使用您的访问令牌来发出`cURL`请求。 

```
curl -H "Authorization: token $token" \
  https://api.linode.com/v4/linode/instances
```

将给您这样的响应:

```
{
  "linodes": [
    {
       "id": 2019697,
       "label": "prod-1",
       "ipv4": "97.107.143.73",
       "ipv6": "2600:3c03::f03c:91ff:fe0a:18c6/64",
       "region": {
          "label": "Newark, NJ",
          "id": "us-east-1a",
          "country": "us"
       },
       "backups": {
          "last_backup": null,
          "snapshot": null,
          "schedule": {
             "day": null,
             "window": null
          },
          "enabled": false
       },
       "status": "running",
       "group": "",
       "hypervisor": "kvm",
       "updated": "2016-11-10T19:38:00",
       "distribution": {
          "disk_minimum": 900,
          "id": "linode/debian8",
          "updated": "2015-04-27T16:26:41",
          "recommended": true,
          "vendor": "Debian",
          "label": "Debian 8.1",
          "architecture": "x86_64"
       },
       "alerts": {
          "io": {
             "threshold": 10000,
             "enabled": true
          },
          "transfer_out": {
             "enabled": true,
             "threshold": 10
          },
          "transfer_in": {
             "enabled": true,
             "threshold": 10
          },
          "transfer_quota": {
             "enabled": true,
             "threshold": 80
          },
          "cpu": {
             "enabled": true,
             "threshold": 90
          }
       },
       "type": [
          {
             "backups_price": 2.5,
             "label": "Linode 2048",
             "disk": 24576,
             "transfer": 2000,
             "vcpus": 1,
             "id": "g5-standard-1",
             "price_hourly": 1,
             "memory": 2048,
             "price_monthly": 10.0,
             "network_out": 125,
             "class": "standard"
          }
       ],
       "transfer_total": 2000,
       "updated": "2016-11-10T19:39:36"
    }
    /* and so on */
  ],
  "page": 1,
  "pages": 1,
  "results": 1
}
```

# 下一节: [利用cURL创建一个Linode_VPS]()