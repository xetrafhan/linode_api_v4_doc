# OAuth 工作流
> by William Smith; last updated May 22nd, 2017

Python 库包括一个针对 Linode 的 OAuth 2 实现的 OAuth 客户端, 使用户可以很容易地对 Linode 进行身份验证, 并授予您的应用程序对其帐户的某些权限。[https://login.linode.com](https://login.linode.com) 和您的应用程序之间的所有通信都由 `LinodeLoginClient` 处理。

# `OAuth Scopes`

`linode.OAuthScopes` 对象包含应用程序可以请求的所有 OAuth 范围。如果您需要访问多个用户的电子邮件地址和使用者用户名, 则需要在您的身份验证请求中包括 OAuth 范围。