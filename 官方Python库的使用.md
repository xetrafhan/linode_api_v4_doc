# Linode Python 入门
> 由威廉史密斯;最后更新2017年10月22日

随着 API V4 的发布, Linode 也发布了一个官方的 Python 库。本指南是使用 Python 库的简单介绍。[官方 Linode Python 库](http://github.com/Linode/python-linode-api)是 `Github` 上的开源软件, 可以安装:
```
pip install linode-api
```

为了向 API 发出请求, 您需要一个 `OAuth 令牌`。您可以在 [cloud.linode.com](cloud.linode.com) 上制作个人访问令牌。

# 连接到API
Python库使用`LinodeClient`类连接到Linode API V4，该类在构造函数中需要一个`OAuth令牌`。

本指南中的所有示例代码均在Python shell中执行。
```
>>> import linode
>>> client = linode.LinodeClient('my-oauth-token')
```

# 创建 Linode 的要求
为了创建一个Linode，我们需要一个Regions（它定义了它将住在哪里）和一个Service（它定义了Linode的大小）。 由于我们不知道API中任何对象的ID，我们将列出它们以查看我们想要的内容：

```
>>> for r in client.get_regions():
...   print(r.label)
...
Dallas, TX
Fremont, CA
Atlanta, GA
Newark, NJ
London, UK
Singapore, SG
Frankfurt, DE
Tokyo 2, JP
Tokyo, JP
```

我们有很多地区需要挑选--让我们先使用第一个, 然后继续前进。

```
>>> region = client.get_regions()[0]
```

对于某种类型, 我们知道我们想要 "Linode 2048"。因为我们知道类型的标签, 我们不需要列出所有`类型`, 我们可以只使用`标签`:

```
>>> type = linode.Type(client, 'g5-standard-1')
>>> type.label
'Linode 2048'
```

在这里，我们通过标签创建`类型`对象 - `标签`用于其他对象使用ID的地方。 以这种方式创建对象可能很有用，但我们也可以在所有将使用的位置使用`标签`来代替类型`对象`。


# 创建 Linode
由于我们拥有制作Linode所需的一切，因此我们可以使用LinodeClient创建一个：
```
>>> l = client.linode.create_instance(type, region)
```
就这样!现在我们有了新的 Linode。让我们检查一下:
```
>>> l.label
'linode263'
>>> l.ipv4
['97.107.143.33']
>>> l.state
'offline'
```
这很好，但是这个Linode是空的，引导它是没有意义的，因为我们创建它没有磁盘和系统。 让我们再次制作另一个Linode，Debian就可以启动它，这样我们就可以使用了。

# 选择系统
我们已经知道如何从API中检索对象，但这次我们想要一些更模糊的东西 - 一个Debian模板。 哪个版本？ 我们有什么选择？ 让我们来看看所有推荐的Debian模板：
```
>>> for d in client.linode.get_distributions(linode.Distribution.vendor == 'Debian', linode.Distribution.recommended == True):
...   print("{}: {}".format(d.label, d.id))
...
Debian 7: linode/debian7
Debian 8.8: linode/debian8
```
Python 库使用 `SQLAlchemy` 类似的过滤语法-任何标记为可的字段都可以通过这种方式进行搜索。我们可以将过滤器连在一起, 进行更复杂的搜索, 甚至在需要时使用 `SQLAlchemy` 运算符, 如 `and_` 和 `or_`。此搜索显示一些选项。因为 debian 8 是最新的 debian 模板, 让我们使用它。因为我们有分配的 id, 所以我们不需要构造一个对象。

# 创建Linode（包括选择系统）
现在, 我们有一个系统, 让我们做一个新的 Linode 运行它!但首先，我们来清理一下我们并不需要的第一个Linode：
```
>>> l.delete()
True
```
删除完毕，我们现在可以创建一个运行Debian 8的新的Linode：
```
>>> l, pw = client.linode.create_instance(type, region, distribution="linode/debian8")
```
这一次，我们用“distribution”关键字参数调用了`create_instance`。这将告诉 API 我们对新 Linode 的要求, 它会做 "正确的事情" 来给你一个工作配置。由于发行版需要root密码，而我们没有提供, 客户帮助我们为我们生成了一个, 以及它返回。让我们启动它，等待它上线：
```
>>> l.boot()
True
>>> while not l.state == 'running':
    ...   pass
  ...
  >>> l.ipv4
['97.107.143.34']
>>> pw
'663Iso_f1y4Zb5xeClY13fBGdeu5l&f3'
```
一旦我们要求启动, 我们等待 Linode 的状态是 "运行"。由于状态是一个 Linode 的 "易变" 属性, 我们可以对它进行轮询。在固定的时间间隔内, 该值将通过后台 API 请求进行更新。一旦该循环存在, 我们可以 ssh in。
```
>>> exit()
$ ssh root@97.107.143.34
The authenticity of host '97.107.143.34 (97.107.143.34)' can't be established.
ECDSA key fingerprint is SHA256:+/zkZlskou45MscmID8Upp5egrBBEssvL0PEx24C5Zw.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added '97.107.143.34' (ECDSA) to the list of known hosts.
root@97.107.143.34's password:

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
root@localhost:~#
```

# 进一步阅读

现在您已经对Python库中的功能和概念进行了概述，请查看Python库参考以获取深入的文档，或者查看 [Linode 示例项目上的安装](https://github.com/linode/python-linode-api/tree/master/examples/install-on-linode), 以了解使用该库的多用户应用程序。